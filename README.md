# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://dkalsan@bitbucket.org/dkalsan/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/dkalsan/stroboskop/commits/159920a8bdb3972d734079b62a65291de288e518

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/dkalsan/stroboskop/commits/07e5fd95ffaa867292f8bf56b14f6781300d9c2c

Naloga 6.3.2:
https://bitbucket.org/dkalsan/stroboskop/commits/600af500e169c13f73420658a753eb0839fbf76c

Naloga 6.3.3:
https://bitbucket.org/dkalsan/stroboskop/commits/c38bc9d136de160d9147a437bb215adbf040b63c

Naloga 6.3.4:
https://bitbucket.org/dkalsan/stroboskop/commits/b63c41532cfd053ebb0f91b51b6ffaba3a6c2801

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/dkalsan/stroboskop/commits/201bcdb3caea2fb599dbdc183dc94665e3a60b75

Naloga 6.4.2:
https://bitbucket.org/dkalsan/stroboskop/commits/8ec079db8ad015aa2ac40d274907169b3292d1fb

Naloga 6.4.3:
https://bitbucket.org/dkalsan/stroboskop/commits/814bf868744ba87f197527703f5756c69ce1abd9

Naloga 6.4.4:
https://bitbucket.org/dkalsan/stroboskop/commits/f8a0c9cf2c7b0b3fd782dd6855bd14c51cf3447f